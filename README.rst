====
fvGP
====

ATTENTION: This repository is no longer up to date and was moved to https://github.com/lbl-camera/fvGP more information https://gpcam.lbl.gov/

===============================

fvGP Copyright (c) 2021, The Regents of the University of California,
through Lawrence Berkeley National Laboratory (subject to receipt of
any required approvals from the U.S. Dept. of Energy). All rights reserved.

If you have questions about your rights to use or distribute this software,
please contact Berkeley Lab's Intellectual Property Office at
IPO@lbl.gov.

NOTICE.  This Software was developed under funding from the U.S. Department
of Energy and the U.S. Government consequently retains certain rights.  As
such, the U.S. Government has been granted for itself and others acting on
its behalf a paid-up, nonexclusive, irrevocable, worldwide license in the
Software to reproduce, distribute copies to the public, prepare derivative 
works, and perform publicly and display publicly, and to permit others to do so.





* Free software: GNU General Public License v3
* Documentation: https://www.overleaf.com/read/nrcyxxsbhvbx


Features
--------

* Python package for highly flexible function-valued Gaussian processes (fvGP)

Credits
-------
This package used the hgdl packagae of David Perryman and Marcus Noack

